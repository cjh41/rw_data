def read_mat(infile):
    """read Matlab input

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    d = loadmat(infile)

    return d


def read_hdf5(infile):
    """read HDF5 input

    :param infile: input file (str)
    :return: mat_file_ref
    """
    import h5py

    f = h5py.File(infile)

    return f


def read_any_mat(infile):
    """ reads in both Matlab v5 and v7.3 files

    :param infile: input file (str)
    :return: mat_dict
    """
    from scipy.io import loadmat
    import h5py

    try:
        mat_file = loadmat(infile)
    except NotImplementedError:
        mat_file = h5py.File(infile)

    mat_dict = dict(mat_file)
    return mat_dict

if __name__ == "__main__":
    read_any_mat('mat_v5.mat')
    read_any_mat('mat_v73.mat')