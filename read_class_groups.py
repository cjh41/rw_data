def read_json(infile):
    """read class groups from JSON file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import json
    with open(infile, 'r') as f:
        class_groups = json.load(f)

    return class_groups


def read_csv(infile):
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    import csv
    with open(infile, 'r') as f:
        c = csv.reader(infile)
        # FIX ME!!
        #class_groups = ????

    return class_groups


def read_csv_numpy(infile):
    import re
    """read class groups from CSV file

    :param infile: input file (str)
    :return: class_groups (dict)
    """
    from numpy import loadtxt
    all_partners = loadtxt(infile, delimiter=', ', dtype='str')

    class_groups = {}

    for partners in all_partners:
        class_groups[partners[0]] = partners[1]

    return class_groups
